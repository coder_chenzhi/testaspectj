package me.zhichen.testcflow;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;




public class Main {
	public static void main(String[] args) {
		foo();
	}
	static void foo() {
		goo();
	}
	static void goo() {
		System.out.println("hi");
	}
}


/*
@Aspect
class Test {
	@Pointcut("execution(void me.zhichen.testcflow.Main.foo())")
	public void fooPC() {}
	
	@Pointcut("execution(void me.zhichen.testcflow.Main.goo())")
	public void gooPC() {}
	
	@Pointcut("call(void java.io.PrintStream.println(String))")
	public void printPC() {}
	
	@Before("cflow(fooPC()) && cflow(gooPC()) && printPC() && !within(Test)")
	public void shouldOcuur() throws Throwable{
		System.out.println("should occur");
	}
	
	@Before("cflow(fooPC() && gooPC()) && printPC() && !within(Test)")
	public void shouldNotOcuur()  {
		System.out.println("should not occur");
	}
}

*/


