package me.zhichen.testTargetAndThis;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

public class TestMove {

	
	public static void move(List<Animal> list){
		for(Animal a : list){
			a.move();
		}
	}
	
	public static void main(String[] args){
		List<Animal> list = new ArrayList<Animal>();
		list.add(new Bird());
		list.add(new Snake());
		move(list);
	}
}


@Aspect
class Test {
	@Pointcut("call(* move(..))")
	public void all() {}
	
	
	@Before("all() && target(TestMove)")
	public void operation(Object TestMove) {
		System.out.println("entering " + TestMove);
	}
}
